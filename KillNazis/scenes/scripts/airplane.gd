extends Area2D

var speed = 400
var gun1
var rocket

func _ready():
	set_process(true)
	add_to_group(game.AIRPLANE_GROUP)
	gun1   = simple_shoot.new(self)
	rocket = rocket_shoot.new(self)

func _process(delta):
	var u = 0; var d = 0; var r = 0; var l = 0;
	
	if Input.is_action_pressed("up"):    u = -1
	if Input.is_action_pressed("down"):  d = 1
	if Input.is_action_pressed("right"): r = 1
	if Input.is_action_pressed("left"):  l = -1
	
	if get_position().y < 80:          u = 0
	if get_position().y > (1080 - 78): d = 0
	if get_position().x > (2160 - 90): r = 0
	if get_position().x < 90:          l = 0
		
	set_position(get_position() + Vector2(0, 1) * speed * delta * (u + d))
	set_position(get_position() + Vector2(1, 0) * speed * delta * (r + l))
	
	# bullet
	if Input.is_action_pressed("shoot one"):
		gun1.shooting()
	gun1.att(delta)
	
	# rocket
	if Input.is_action_pressed("shoot two"):
		rocket.shooting()
	rocket.att(delta)
	
class simple_shoot:
	extends Node
	var airplane
	var pre_bullet = preload("res://scenes/bullet.tscn")
	
	var intervalbullet   = 0.3
	var last_shootbullet = 0
	
	func _init(airplane):
		self.airplane = airplane
		
	func shooting():
		if last_shootbullet <= 0:
			shootingbullet(airplane.get_node("bulletPos"))
			last_shootbullet = intervalbullet
	
	func att(delta):
		if last_shootbullet > 0: last_shootbullet -= delta
		
	func shootingbullet(node):
		var shoot = pre_bullet.instance()
		shoot.set_global_position(node.get_global_position())
		airplane.get_owner().add_child(shoot)

class fastest_shoot:
	extends Node
	var airplane
	var pre_bullet = preload("res://scenes/bullet.tscn")
	
	var intervalbullet   = 0.2
	var last_shootbullet = 0
	
	func _init(airplane):
		self.airplane = airplane
		
	func shooting():
		if last_shootbullet <= 0:
			shootingbullet(airplane.get_node("bulletPos"))
			last_shootbullet = intervalbullet
	
	func att(delta):
		if last_shootbullet > 0: last_shootbullet -= delta
		
	func shootingbullet(node):
		var shoot = pre_bullet.instance()
		shoot.set_global_position(node.get_global_position())
		airplane.get_owner().add_child(shoot)
		
class rocket_shoot:
	extends Node
	var airplane
	var pre_rocket = preload("res://scenes/rocket.tscn")
	
	var rocketlife       = 5
	var intervalrocket   = 0.5
	var last_shootrocket = 0
	var intervalsong     = 0.3
	var last_songplayed  = 0
	
	func _init(airplane):
		self.airplane = airplane
	
	func shooting():
		if last_shootrocket <= 0 && rocketlife >= 1:
			rocketlife -= 1
			shootingrocket(airplane.get_node("rocketPos"))
			last_shootrocket = intervalrocket
			last_songplayed = intervalsong
		elif rocketlife < 1 && last_songplayed <= 0:
			airplane.get_tree().get_root().set_disable_input(true)
			airplane.get_node("noRocket").play()
			last_songplayed = intervalsong
			
	func att(delta):
		if last_shootrocket > 0: last_shootrocket -= delta
		if last_songplayed > 0: last_songplayed -= delta
			
	func shootingrocket(node):
		var shoot = pre_rocket.instance()
		shoot.set_global_position(node.get_global_position())
		airplane.get_owner().add_child(shoot)
		
func set_gun(value):
	if value == 1:
		gun1 = fastest_shoot.new(self)
		
func set_rocket_life(value):
	if value == 1:
		rocket = rocket_shoot.new(self)
		
	
