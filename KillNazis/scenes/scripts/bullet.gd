extends Area2D

# Declare member variables here. Examples:
var spe = 1300

# Called when the node enters the scene tree for the first time.
func _ready():
	
	set_process(true)
	get_node("shooting").play()
	
	pass
	
func _process(delta):
	
	set_position(get_position() + Vector2(1, 0) * spe * delta)
	
	if get_position().x > 2160 + 30:
		queue_free()
		
		pass
	
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.



func _on_bullet_area_entered(area):
	if area.is_in_group(game.ENEMY_GROUP):
		if area.has_method("take_damage"):
			area.take_damage(1)
		else:
			area.queue_free()
		queue_free()
	
	pass 
