extends Area2D

# Declare member variables here. Examples:
var speed = 800
var life = 4

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group(game.ENEMY_GROUP)
	set_process(true)
	
	pass # 
	
func _process(delta):
	set_position(get_position() + Vector2(-1, 0) * speed * delta)
	
	if get_position().x < -65:
		queue_free()
	pass

func take_damage(value):
	life -= value
	get_node("anim").play("hit")
	get_node("takegamage").play()
	if life <= 0:
		die()
				
	pass
	
#Func to make the animy die
func die():
	set_process(false)
	set_z_index(10)
	remove_from_group(game.ENEMY_GROUP)
	get_node("anim").play("destroy")
	get_node("explosion").play()
	game.getCamera().shake()
	
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.

 