extends Area2D

# Declare member variables here. Examples:
var speed = 0
var acceleration = 20
var maxspeed = 1500

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("anim").play("flying")
	get_node("inFire").play()
	
	set_process(true)

func _process(delta):
	
	set_position(get_position() + Vector2(1, 0) * speed * delta)
	
	if speed < maxspeed:
		speed += acceleration
	
	if get_position().x > 2160 + 60:
		queue_free()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _on_rocket_area_entered(area):
	if area.is_in_group(game.ENEMY_GROUP):
		if area.has_method("take_damage"):
			area.take_damage(4)
		else:
			area.queue_free()
		queue_free()
			
