extends Node

# Declare member variables here. Examples:
var pre_nazis = preload("res://scenes/nazis.tscn")
var interval = 6
var width_display
var height_display

# Called when the node enters the scene tree for the first time.
func _ready():
	width_display = OS.get_window_size().x
	height_display = OS.get_window_size().y
	
	set_process(true)
	pass 

func _process(delta):
	if interval > 0:
		interval -= delta
	else:
		interval = rand_range(0.3, 6)
		var nazis = pre_nazis.instance()
		var height_sprite = 73
		
		var y_nazis = rand_range(height_sprite, height_display + height_sprite * 4)
		nazis.set_position(Vector2(width_display * 2, y_nazis))
		set_pause_mode(true)
		get_owner().add_child(nazis)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.